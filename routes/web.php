<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@list')->name('main');

Route::get('/login', function () {
	return view('auth.login');
})->name('login_form');

Route::get('/register', function () {
	return view('auth.register');
})->name('register_form');

Route::post('/login/do','AuthController@doLogin')->name('dologin');

Route::post('/register/do','AuthController@create')->middleware('validateRegister')->name('register');

Route::get('/logout', 'AuthController@logout')->name('logout');


Route::get('/dashboard', 'BlogController@list')->middleware('checkLogin')->name('dashboard');

Route::get('/blog/{:id}', 'BlogController@view')->name('viewblog');

Route::get('/blog', function () {
	return view('blog.form');
})->name('blog_form');

Route::post('/blog/post','BlogController@create')->middleware('checkLogin')->name('postblog');
Route::get('/blog/update/{id}','BlogController@updateBlogForm')->middleware('checkLogin')->name('updateblog');
Route::post('/blog/update','BlogController@update')->middleware('checkLogin')->name('updateblogpost');
Route::get('/blog/delete/{id}','BlogController@delete')->middleware('checkLogin')->name('deleteblog');


