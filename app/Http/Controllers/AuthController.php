<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersModel;
use Crypt;
use Session;

class AuthController extends Controller
{
	public function __construct()
    {
        
    }
	
	function create(Request $request){
		
		$name = $request->get('name');		
		$email = $request->get('email');
		$password = $request->get('password');
		
		$user = new UsersModel;
		$user->name = $name;
		$user->email = $email;
		$user->password = Crypt::encrypt($password);
		$user->save();
		
		return redirect()->back()->with('message', 'User Created Successfully');
			
	}
	
	function doLogin(Request $request){
		
		$email = $request->get('email');
		$password = Crypt::encrypt($request->get('password'));
		
		$user = UsersModel::where('email', $email)->first();
			   
		if ( isset($user->password) && Crypt::decrypt($user->password) == $password ) ;
		{			
			Session::put('user_session', ['user_id'=>$user->id,'name'=>$user->name,'email'=>$user->email]);
		}
		
		return redirect('dashboard');
	}
	
	function logout(){
		Session::flush();
		return redirect('/');
	}
	
}
