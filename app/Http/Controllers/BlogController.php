<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogModel;
use App\UsersModel;

use Session;

class BlogController extends Controller
{
	public function __construct()
    {
	}
	
	function list(){
		
		$blogs = \App\BlogModel::orderBy('created_at','desc')->get();
		
		return view('blog.dashboard')->with('blogs',$blogs);
	}
	
	function view(Request $request){
		$id = $request->get('id');
		
		$validatedData = $request->validate([
			'id' => 'required'
		]);
	}
	
	function updateBlogForm($id){
		
		$blog = \App\BlogModel::where('id',$id)->first();
		
		return view('blog.edit')->with('blog',$blog);
	}
	
	function create(Request $request){
		
		$title = $request->get('title');		
		$description = $request->get('description');
		
		$validatedData = $request->validate([
			'title' => 'required|unique:blog|max:100',
			'description' => 'required',
		]);
		
		
		$sessionData = Session::get('user_session');
				
		$blog = new BlogModel;
		$blog->title = $title;
		$blog->description = $description;
		$blog->user_id = $sessionData['user_id'];
		$blog->save();
		
		return redirect()->back()->with('message', 'Blog Created Successfully');
			
	}
	
	function update(Request $request){
		
		$id = $request->get('id');
		$title = $request->get('title');		
		$description = $request->get('description');
		
		$validatedData = $request->validate([
			'id' => 'required',
		]);
				
		$blog = \App\BlogModel::where('id',$id)->update(['title'=>$title,'description'=>$description]);
		
		return redirect()->back()->with('message', 'Blog updated Successfully');
			
	}
	
	function delete($id){
		
		$blog = \App\BlogModel::where('id',$id)->delete();
		
		return redirect()->back()->with('message', 'Blog deleted Successfully');
			
	}
	
	
}
