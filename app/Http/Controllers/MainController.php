<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogModel;

use Session;

class MainController extends Controller
{
	public function __construct()
    {
	}
	
	function list(){
		
		$blogs = \App\BlogModel::orderBy('created_at','desc')->get();
		
		return view('main')->with('blogs',$blogs);
	}
	
}
