<?php

namespace App\Http\Middleware;

use Closure;

class ValidateRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name = $request->get('name');		
		$email = $request->get('email');
		$password = $request->get('password');
		
		$exit = false;
		if( empty(trim($name)) ){
			$exit = true;
		} else if ( empty(trim($email)) ) {
			$exit = true;
		} else if ( empty(trim($password)) ) {
			$exit = true;
		}
		
		if ( $exit ) {
			return redirect()->back()->with('message', 'Invalid credentials');
		}
		
		return $next($request);
    }
}
