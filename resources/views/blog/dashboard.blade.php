@extends('layout.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
					@foreach($blogs as $blog)
                    <div class="row">
						<div class="card">
							<h2>{{$blog->title}}</h2>
							<p>{{$blog->description}}</p>
							
							<a href="{{route('updateblog',$blog->id)}}">UPDATE</a>
							<a href="{{route('deleteblog',$blog->id)}}">DELETE</a>
						</div>
					</div>
					@endforeach  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection